//
//  ViewController.m
//  FoodApp
//
//  Created by Daniel Ungerfält on 08/03/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

#import "ViewController.h"
#import "FoodDetailViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *searchText;
@property (weak, nonatomic) IBOutlet UITextView *resultText;
@property (weak, nonatomic) IBOutlet UILabel *headLine1;
@property (weak, nonatomic) IBOutlet UILabel *headLine2;
@property (weak, nonatomic) IBOutlet UILabel *itemIsMissingText;

@end

@implementation ViewController
- (IBAction)searchButton:(id)sender {
   // [self testMethod];
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@",self.searchText.text];
    NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escaped];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        NSMutableArray *resultItems = [NSJSONSerialization JSONObjectWithData:data  options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError) {
            NSLog(@"falied to parse data: %@", jsonParseError);
        }
        
        if(resultItems.count == 0) {
            NSLog(@"Det finns ingen vara med det namnet");
        } else {
            self.foodName = resultItems;
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            if(resultItems.count > 0){
                self.itemIsMissingText.text = @"";
                self.searchText.text = @"";
            [self performSegueWithIdentifier:@"Result" sender:self];
            } else {
                self.itemIsMissingText.text = @"Varan finns ej";
                self.searchText.text = @"";
            }
        });
    }];
    [task resume];
}

-(void)viewDidLayoutSubviews{
    [self animationText];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.foodName = [[NSMutableArray alloc]init].mutableCopy;
    self.foodNumber = [[NSNumber alloc]init];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    FoodTableViewController *destination = [segue destinationViewController];
    destination.foodName =  self.foodName;
}

-(void)animationText {
    [UIView animateWithDuration:1.0 delay:0 options:kNilOptions animations:^{
            self.headLine1.center = CGPointMake(self.headLine1.center.x, 300);
        
    } completion:^(BOOL finished) {
        
        }];
        [UIView animateWithDuration:1.0 delay:0.2 options:kNilOptions
            animations:^{
                         self.headLine2.center = CGPointMake(self.headLine2.center.x, 300);
                         
                     }completion:^(BOOL finished) {
                         
                     }];
    }
@end
