//
//  FoodDetailViewController.m
//  FoodApp
//
//  Created by Daniel Ungerfält on 08/03/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

#import "FoodDetailViewController.h"

@interface FoodDetailViewController ()
@property (weak, nonatomic) IBOutlet UITextView *nutrientText;
@property (weak, nonatomic) IBOutlet UILabel *goodieValue;
@property (nonatomic) NSNumber *fat;
@property (nonatomic) NSNumber *carbohydrates;
@property (nonatomic) NSNumber *calories;
@property (nonatomic) NSNumber *protein;
@property (nonatomic) NSNumber *vitaminC;
@property (weak, nonatomic) IBOutlet UILabel *comment;
@end
UIDynamicAnimator *animator;
UIGravityBehavior *gravity;
UICollisionBehavior *collision;
UIDynamicItemBehavior *bounce;

@implementation FoodDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", self.foodNumber];
    NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escaped];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
                self.fat = [result objectForKey:@"nutrientValues"][@"fat"];
                self.protein = [result objectForKey:@"nutrientValues"][@"protein"];
                self.carbohydrates = [result objectForKey:@"nutrientValues"][@"carbohydrates"];
                self.vitaminC = [result objectForKey:@"nutrientValues"][@"vitaminC"];
                self.calories = [result objectForKey:@"nutrientValues"][@"energyKcal"];
                self.nutrientText.text = [NSString stringWithFormat:@"Kalorier: %@ \n Fett: %@ \n Protein: %@ \n Kolhydrater: %@ \n Vitamin C: %@ ",self.calories, self.fat, self.protein, self.carbohydrates,self.vitaminC];
                [self calculateGoodieValue];
            
                   });
    }];
   
    [task resume];
    
    [self gravity];
}

-(void)calculateGoodieValue {
    double total =  self.fat.doubleValue + self.calories.doubleValue + self.carbohydrates.doubleValue;
          self.goodieValue.text = [NSString stringWithFormat:@"Nyttighetsvärde: %.1f",total];
    if (total < 150) {
        self.comment.text = @"Perfekt under deff!";
    } else if (total > 100 && total <501) {
        self.comment.text = @"Lagom är bra!";
    } else if (total < 900) {
        self.comment.text = @"Kaloribomb!";
    }

    
}

-(void)gravity {
    animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    gravity = [[UIGravityBehavior alloc] initWithItems:@[self.comment,self.goodieValue]];
    [animator addBehavior:gravity];
    collision = [[UICollisionBehavior alloc] initWithItems:@[self.comment,self.goodieValue]];
    collision.translatesReferenceBoundsIntoBoundary = YES;
    [animator addBehavior:collision];
    bounce = [[UIDynamicItemBehavior alloc] initWithItems:@[self.comment,self.goodieValue]];
    bounce.elasticity = 0.7;
    [animator addBehavior:bounce];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
