//
//  ViewController.h
//  FoodApp
//
//  Created by Daniel Ungerfält on 08/03/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodTableViewController.h"

@interface ViewController : UIViewController
@property (strong, nonatomic) NSMutableArray *foodName;
@property (nonatomic) NSNumber *foodNumber;


@end

