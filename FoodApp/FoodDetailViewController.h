//
//  FoodDetailViewController.h
//  FoodApp
//
//  Created by Daniel Ungerfält on 08/03/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodDetailViewController : UIViewController
@property (nonatomic) NSMutableArray *foodName;
@property NSNumber *foodNumber;
@end
