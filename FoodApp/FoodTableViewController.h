//
//  FoodTableViewController.h
//  FoodApp
//
//  Created by Daniel Ungerfält on 08/03/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "FoodDetailViewController.h"

@interface FoodTableViewController : UITableViewController
@property (strong, nonatomic) NSMutableArray *foodName;
@property (nonatomic) NSNumber *foodNumber;
@end
